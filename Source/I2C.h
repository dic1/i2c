/**
 * I2C.c
 * Description:
 *	I2C ( TWI ) interface for the Atmega644X micro controller.
 * Author : ObeE
 */

#ifndef I2C_H
#define I2C_H

#include "HtlStddef.h"
#include "Logger.h"
#include "twi.h"
#include <avr/io.h>

/**
 *  Function: I2CInit
 *
 *  Description:
 *    This function calculates the prescaler(TWPS) and bit rate
 *	  register(TWBR) value. By setting the bit rate register to
 *	  its value we also set the Frequency. This function also activates
 *	  the internal pull-up resistors on the SDA (PC1) and SCL (PC0) pins.
 *
 *  Parameters:
 *    aBitrate		-
 *    aClkFrequency	- The CPU frequency.
 *
 *	Returns:
 *	  ETRUE if the initialization process was successful, else EFALSE.
 */
TBool I2CInit(unsigned long aBitrate, unsigned long aClkFrequency);

/**
 *  Function: I2CWrite
 *
 *  Description:
 *    This function sends the slave the address, checks the
 *    acknowledge bit and also sends a data byte to TWDR register.
 *
 *  Parameters:
 *    aAddress	- The address of the slave you want to write to.
 *    aBuffer	- A pointer to the first element of an array.
 *    aSize		- The size of the aBuffer array.
 *
 *	Returns:
 *    ETRUE if all the data from the buffer was successfully sent to the slave,
 *	  in case the data was only partially sent or the entire
 *	  operation failed, EFALSE  gets returned.
 */
TBool I2CWrite(unsigned char aAddress,
               unsigned char* aBuffer,
               unsigned int aSize);

/**
 *  Function: I2CRead
 *
 *  Description:
 *    This function sends the slave the address, checks the acknowledge
 *	  bit and also reads the upper five bits from the TWSR register.
 *
 *  Parameters:
 *    aAddress	- The address of the slave you want to read data from.
 *    aBuffer	- A pointer to the first element of an array.
 *    aSize		- The size of the aBuffer array.
 *
 *	Returns:
 *    ETRUE if all the data from the slave was read successfully, in case the
 *	  data was only partially read or the entire operation
 *	  failed, EFALSE  gets returned.
 */
TBool I2CRead(unsigned char aAddress,
              unsigned char* aBuffer,
              unsigned int aSize);

#endif  // I2C_H
