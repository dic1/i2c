/*
 * I2C.c
 * Description:
 *		I2C ( TWI ) interface for the Atmega644X micro controller.
 * Author : ObeE
 */

#include "I2C.h"

// Setting all Pre-Scaler Values
static const unsigned char I2CPrescalerValues[] = {1, 4, 16, 64};

/******************** Private Function Declarations ********************/

/**
 *  Function: I2CStart
 *  Description:
 *    Startup a connection to the slave.
 */
static TBool I2CStart(void);

/**
 *  Function: I2CStop
 *  Description:
 *    Stop the connection with the slave.
 */
static void I2CStop(void);

/**
 *  Function: I2CSendAddress
 *
 *  Description:
 *    This function sends the address to the slave and
 *	 checks the acknowledge bit.
 *
 *  Parameters:
 *    aAddress - The address of the slave, with which you want to operate.
 */
static TBool I2CSendAddress(unsigned char aAddress);

/**
 *  Function: I2CSendByte
 *
 *  Description:
 *   This function sends a data byte to the TWDR register and
 *	checks whether or not an acknowledge came back.
 *
 *  Parameters:
 *    aByte	- The data to be written to the slave.
 */
static TBool I2CSendByte(unsigned char aByte);

/**
 *  Function: I2CReadByte
 *
 *  Description:
 *	 This function receives a byte from the TWDR register and
 *	 checks whether or not an acknowledge came back.
 *
 *  Parameters:
 *    aByte	- A pointer to the variable where you want the data
 *			  to be written to.
 */
static TBool I2CReadByte(unsigned char* aByte);

/**
 *  Function: I2CReadByte
 *
 *  Description:
 *	 This function receives a byte from the TWDR register, but
 *	 the last byte does not return an acknowledge bit.
 *
 *  Parameters:
 *    aByte	- A pointer to the variable where you want the data
 *			  to be written to.
 */
static TBool I2CReadLastByte(unsigned char* aByte);

/******************** Public Function Definitions ********************/
TBool I2CInit(unsigned long aBitrate, unsigned long aClkFrequency) {
  unsigned long _bitrate;

  // Calculate Prescaler value ( TWPS ) and bit rate register ( TWBR) value.
  // mit Bitrate register TWBR < 256  und Prescaler Value TWPS setzten wir
  // unsere Sende-Frequenz

  // searches appropriate Prescaler
  unsigned char prescalerIndex;
  for (prescalerIndex = 0; prescalerIndex < sizeof(I2CPrescalerValues);
       prescalerIndex++) {
    _bitrate = ((aClkFrequency / aBitrate) - 16) /
               (2 * I2CPrescalerValues[prescalerIndex]);

    if (_bitrate < 256) {
      // Activate internal pull-ups for the SDA and SCL pin.
      PORTC |= (1 << PORTC0) | (1 << PORTC1);
      DDRC &= ~(1 << DDC0) & ~(1 << DDC1);

      // activates the chosen Prescaler.
      TWSR |= (prescalerIndex << TWPS0);
      TWBR = _bitrate;

      break;
    }
  }

  if (_bitrate > 255)
    return EFALSE;

  return ETRUE;
}

TBool I2CWrite(unsigned char aAddress,
               unsigned char* aBuffer,
               unsigned int aSize) {
  unsigned int i;

  if (!I2CStart())
    return EFALSE;

  if (!I2CSendAddress(aAddress & TW_WRITE)) {
    I2CStop();
    return EFALSE;
  }

  for (i = 0; i < aSize; i++) {
    if (!I2CSendByte(aBuffer[i])) {
      I2CStop();
      return EFALSE;
    }
  }

  I2CStop();

  return ETRUE;
}

TBool I2CRead(unsigned char aAddress,
              unsigned char* aBuffer,
              unsigned int aSize) {
  unsigned int i;

  // Startup connection
  if (!I2CStart())
    return EFALSE;

  // See if SLAVE successfully acknowledged
  if (!I2CSendAddress(aAddress | TW_READ)) {
    I2CStop();
    return EFALSE;
  }

  // Fill aBuffer with data
  for (i = 0; i < aSize - 1; i++) {
    if (!I2CReadByte(&aBuffer[i])) {
      I2CStop();
      return EFALSE;
    }
  }

  if (!I2CReadLastByte(&aBuffer[aSize - 1])) {
    I2CStop();
    return EFALSE;
  }

  // Stop connection after reading the data
  I2CStop();

  return ETRUE;
}

/******************** Private Function Definitions ********************/

static TBool I2CStart(void) {
  TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTA);

  while (!(TWCR & (1 << TWINT)))
    ;

  if ((TWSR & TW_STATUS_MASK) != TW_START &&
      (TWSR & TW_STATUS_MASK) != TW_REP_START)
    return EFALSE;

  return ETRUE;
}

static void I2CStop(void) {
  TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
}

static TBool I2CSendAddress(unsigned char aAddress) {
  TWDR = aAddress;

  TWCR = (1 << TWINT) | (1 << TWEN);

  while (!(TWCR & (1 << TWINT)))
    ;

  // Check wether Slave successfully acknoweledged
  if (aAddress & TW_READ) {
    // Check for Master RECEIVE Slave Acknowledge
    if ((TWSR & TW_STATUS_MASK) != TW_MR_SLA_ACK)
      return EFALSE;
  } else {
    // Check for Master TRANSMIT Slave Acknowledge
    if ((TWSR & TW_STATUS_MASK) != TW_MT_SLA_ACK)
      return EFALSE;
  }

  return ETRUE;
}

static TBool I2CSendByte(unsigned char aByte) {
  TWDR = aByte;

  TWCR = (1 << TWINT) | (1 << TWEN);

  while (!(TWCR & (1 << TWINT)))
    ;

  if ((TWSR & TW_STATUS_MASK) != TW_MT_DATA_ACK)
    return EFALSE;

  return ETRUE;
}

static TBool I2CReadByte(unsigned char* aByte) {
  TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);

  while (!(TWCR & (1 << TWINT)))
    ;

  if ((TWSR & TW_STATUS_MASK) != TW_MR_DATA_ACK)
    return EFALSE;

  (*aByte) = TWDR;

  return ETRUE;
}

static TBool I2CReadLastByte(unsigned char* aByte) {
  TWCR = (1 << TWINT) | (1 << TWEN);

  while (!(TWCR & (1 << TWINT)))
    ;

  if ((TWSR & TW_STATUS_MASK) != TW_MR_DATA_NACK)
    return EFALSE;

  (*aByte) = TWDR;

  return ETRUE;
}
