/************************************************************************
File: RingBuffer.h
Description:
Thread / Interrupt safe ringbuffer for data transfer. The read
and write process can be interrupted at every assembler command.
************************************************************************/

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <stdlib.h>

#include "HtlStddef.h"

typedef struct RingBufferStruct* TRingBuffer;

/**
 * Function: RingBufferCreate
 *
 * Description:
 *	Allocate memory for a new RingBuffer, with a buffer size of aSize.
 *
 * Parameters:
 *	aSize - The size of the internal buffer,
 *	that is used for storing the data.
 *
 * Returns:
 *	A pointer to the newly allocated RingBuffer struct.
 */
TRingBuffer RingBufferCreate(unsigned char aSize);

/**
 * Function: RingBufferDestroy
 *
 * Description:
 *	Free the memory that was allocated for aRingBuffer.
 *
 * Parameters:
 *	aRingBuffer - The RingBuffer you want to destroy.
 */
void RingBufferDestroy(TRingBuffer aRingBuffer);

/**
 * Function: RingBufferWrite
 *
 * Description:
 *	Write aByte to the RingBuffers internal buffer.
 *
 * Parameters:
 *	aRingBuffer - The RingBuffer you want to write to.
 *	aByte - The data byte you want to write.
 *
 * Returns:
 *	EFALSE, if internal buffer is full else ETRUE on success.
 */
TBool RingBufferWrite(TRingBuffer aRingBuffer, unsigned char aByte);

/**
 * Function: RingBufferRead
 *
 * Description:
 *	Read the next byte from the RingBuffers internal buffer.
 *
 * Parameters:
 *	aRingBuffer - The RingBuffer you want to read from.
 *	aByte - The address of your variable where the read
 *	byte should be stored.
 *
 * Returns:
 *	EFALSE, if there is no data to be read else ETRUE on success.
 */
TBool RingBufferRead(TRingBuffer aRingBuffer, unsigned char* aByte);

#endif
