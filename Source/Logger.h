/**
 * File: Logger.h
 *
 * Description:
 *	Test different methods for loggings.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum {
  LOG_LEVEL_DEBUG = 1,
  LOG_LEVEL_INFO = 2,
  LOG_LEVEL_WARNING = 3,
  LOG_LEVEL_ERROR = 4,
} TLogLevel;

typedef struct {
  const char* file;
  const char* function;
  unsigned int line;
  TLogLevel logLevel;
} TLogEvent;

typedef void (*TLogCallback)(const TLogEvent* logEvent,
                             const void* userData,
                             const char* message);

/**
 * Function: LogInit
 *
 * Description:
 *	Initialize the Logger. This has to be called once
 *	before any Log functions can be used.
 */
void LogInit(void);

/**
 * Function: LogSetLogCallback
 *
 * Description:
 *	Set a TLogCallback that gets called when
 *	a LogEvent occurs.
 *
 * Parameters:
 *	callback - The custom TLogCallback, which you want to get fired on a
 *TLogEvent. userData - Custom data you might want to pass as a parameter.
 */
void LogSetLogCallback(TLogCallback callback, const void* userData);

void LogErrorFormatedExtented1(const char* format, ...);

/**
 * Function: _LogEvent
 *
 * Description:
 *	Internal Log function, this should not be used.
 */
void _LogEvent(const char* file,
               const char* func,
               unsigned int line,
               TLogLevel logLevel,
               const char* message);

// This should be defined in the development system.
#define LOG_DEBUG

#ifdef _DEBUG

#define LOG1(message) printf("%s Line %4u: %s\n", __FILE__, __LINE__, message)
#define LOG2(message) \
  printf("%s %s Line %4u: %s\n", __FILE__, __func__, __LINE__, message)

#ifdef LOG_DEBUG
#define LogDebug(message) \
  _LogEvent(__FILE__, __func__, __LINE__, LOG_LEVEL_DEBUG, message)
#endif  // LOG_DEBUG

#define LogInfo(message) \
  _LogEvent(__FILE__, __func__, __LINE__, LOG_LEVEL_INFO, message)
#define LogWarning(message) \
  _LogEvent(__FILE__, __func__, __LINE__, LOG_LEVEL_WARNING, message)

#else

#define LOG1(message)
#define LOG2(message)

#define LogDebug(message)
#define LogInfo(message)
#define LogWarning(message)

#endif  // _DEBUG

#define LogError(message) \
  _LogEvent(__FILE__, __func__, __LINE__, LOG_LEVEL_ERROR, message)

#endif  // LOGGER_H
