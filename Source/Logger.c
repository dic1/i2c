/**
 * File: Logger.c
 *
 * Description:
 *	Test different methods for loggings.
 */

#include "Logger.h"

/******************** Declarations of Private Structs ********************/

typedef struct {
  TLogCallback callback;
  const void* userData;
} TLogger;

TLogger logger;

/******************** Definition of Public Functions ********************/

void LogInit(void) {
  memset(&logger, 0, sizeof(logger));
}

void LogSetLogCallback(TLogCallback callback, const void* userData) {
  logger.callback = callback;
  logger.userData = userData;
}

/******************** Definition of Private Functions ********************/

void _LogEvent(const char* file,
               const char* func,
               unsigned int line,
               TLogLevel logLevel,
               const char* message) {
  if (NULL == logger.callback)
    return;

  TLogEvent logEvent;

  memset(&logEvent, 0, sizeof(logEvent));

  logEvent.file = file;
  logEvent.function = func;
  logEvent.line = line;
  logEvent.logLevel = logLevel;

  logger.callback(&logEvent, logger.userData, message);
}

void LogErrorFormatedExtented1(const char* format, ...) {
#ifdef _DEBUG

  if (NULL == logger.callback)
    return;

#define MESSAGE_SIZE 256
  va_list args;
  TLogEvent logEvent;
  char message[MESSAGE_SIZE];

  memset(&logEvent, 0, sizeof(logEvent));

  va_start(args, format);
  vsprintf(message, format, args);
  va_end(args);

  logger.callback(&logEvent, logger.userData, message);

#endif  // _DEBUG
}
