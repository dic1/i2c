#include "HtlStddef.h"
#include "I2C.h"
#include "Logger.h"
#include "RS232.h"
#include <avr/io.h>

#define ADDRESS_PCF8583_READ 0xA1
#define ADDRESS_PCF8583_WRITE 0xA0

#define I2C_BAUDRATE 10000UL
#define RS232_BAUDRATE 57600U

#define DELAY() DelayMs(1000)

TRS232 globalRS232;

int customPrintfPut(char character, FILE* file);
void logCallBack(const TLogEvent* logEvent,
                 const void* userData,
                 const char* message);

int testI2C_RTC(void);
int testReadRegisterSeconds(void);

int main(void) {
  LogInit();
  LogSetLogCallback(logCallBack, NULL);

  int result = 0;

  result = testI2C_RTC();
  // result = testReadRegisterSeconds();

  return result;
}

int customPrintfPut(char character, FILE* file) {
  RS232Send((TRS232)globalRS232, (unsigned char*)&character, 1);
  return 1;
}

void logCallBack(const TLogEvent* logEvent,
                 const void* userData,
                 const char* message) {
  char* tag;

  switch (logEvent->logLevel) {
    case LOG_LEVEL_DEBUG:
      tag = "DEBUG:\n";
      break;

    default:
    case LOG_LEVEL_INFO:
      tag = "INFO:\n";
      break;

    case LOG_LEVEL_WARNING:
      tag = "WARNING:\n";
      break;

    case LOG_LEVEL_ERROR:
      tag = "ERROR:\n";
      break;
  }

  printf("%sFile: %s | Line: %u | Function: %s\nMessage: %s\n", tag,
         logEvent->file, logEvent->line, logEvent->function, message);
}

int testI2C_RTC(void) {
  TRS232Config config;

  config.sendBufferSize = 200;
  config.receiveBufferSize = 10;
  config.baudrate = RS232_BAUDRATE;
  globalRS232 = RS232Create(ERS232_NO_0, F_CPU, config);

  fdevopen(customPrintfPut, NULL);

  if (!I2CInit(I2C_BAUDRATE, F_CPU)) {
    LogError("I2CInit failed");
    DELAY();
    return -1;
  }

  unsigned char registerAddress = 0x01;  // hundreds of a second
  unsigned char timeData[5];
  unsigned char timeString[20];

  while (1) {
    if (!I2CWrite(ADDRESS_PCF8583_WRITE, &registerAddress, 1)) {
      LogError("I2CWrite failed");
      DELAY();
    }

    // Read 5 bytes because we want the hundreds of second, seconds, minutes and
    // hours
    if (!I2CRead(ADDRESS_PCF8583_READ, timeData, 4)) {
      LogError("I2CRead failed");
      DELAY();
    }

    sprintf((char*)timeString, "%02x:%02x:%02x.%02x", timeData[3], timeData[2],
            timeData[1], timeData[0]);

    timeString[19] = '\0';

    LogInfo((const char*)timeString);
    DELAY();
  }

  RS232Destroy(globalRS232);

  return 0;
}

int testReadRegisterSeconds(void) {
  TRS232Config config;

  config.sendBufferSize = 200;
  config.receiveBufferSize = 10;
  config.baudrate = RS232_BAUDRATE;
  globalRS232 = RS232Create(ERS232_NO_0, F_CPU, config);

  fdevopen(customPrintfPut, NULL);

  if (!I2CInit(I2C_BAUDRATE, F_CPU)) {
    LogError("I2CInit failed");
    DELAY();
    return -1;
  }

  unsigned char registerAddress = 0x02;  // seconds
  unsigned char timeData[5] = {0};
  unsigned char timeString[20] = {0};

  while (1) {
    if (!I2CWrite(ADDRESS_PCF8583_WRITE, &registerAddress, 1)) {
      LogError("I2CWrite failed");
      DELAY();
    }

    // Only read 1 byte, because only the seconds interest us
    if (!I2CRead(ADDRESS_PCF8583_READ, timeData, 1)) {
      LogError("I2CRead failed");
      DELAY();
    }

    sprintf((char*)timeString, "%02x seconds", timeData[0]);

    timeString[19] = '\0';

    LogInfo((const char*)timeString);
    DELAY();
  }

  RS232Destroy(globalRS232);

  return 0;
}

